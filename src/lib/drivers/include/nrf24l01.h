/*
This is a driver for the Nordic NRF24L01(+) device
Copyright (C) 2013 Christian Ege <k4230r6@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/**
 *  @file
 *
 *****************************************************************************
 * @title   nrf24l01.h
 * @author  Christian Ege <k4230r6@gmail.com
 *
 *******************************************************************************
 */

#include <sys/types.h>
#include <stdbool.h>

#ifndef NRF24L01_H__
#define NRF24L01_H__

#ifdef __cplusplus
extern "C"
{
#endif

/** @def defines the amount of available pipes */
#define NRF24L01_NR_PIPES 6
/** @def defines the length of an address we are using fixed address length */
#define NRF24L01_ADDR_LEN 5
/** @def defines the maximum payload size */
#define NRF24L01_MAX_PAYLOAD_BYTE 32
/** @def helper to limit the available spi channels */
#define NRF24L01_SPI_CHANNELS 2 /* TODO ce: move this to the SPI driver */

/**
 * @brief Definition of the air data rates
 */
typedef enum {
	NRF24L01_250KBPS, /**< 250 kbit/s data rate */
	NRF24L01_1MBPS,   /**<   1 Mbit/s data rate */
	NRF24L01_2MBPS    /**<   2 Mbit/s data rate */
} NRF24L01_Air_Data_Rate_t;

/**
 * @brief Definition of the chip output power used in TX mode
 */
typedef enum {
	NRF24l01_n18DBM = 0, /**< lowest power -18dBm DC current consumption  7.0 mA */
	NRF24l01_n12DBM = 1, /**< low power    -12dBm DC current consumption  7.5 mA */
	NRF24l01_n06DBM = 2, /**< mid power     -6dBm DC current consumption  9.0 mA */
	NRF24l01_p00DBM = 3, /**< high power     0dBm DC current consumption 11.3 mA */
}NRF24L01_Output_Power_t;

/* @typedef forward declaration of private data */
typedef struct NRF24L01 NRF24L01_t;

/**
 * @brief Transmit status information
 */
typedef struct {
	uint8_t plos_cnt;  /**<	counts the total number of retransmissions since the last channel change */
	uint8_t arc_cnt;   /**<	counts the number of retransmissions for the current transaction */
}NRF24L01_tx_status_t;

/**
 * @brief Interrupt return status
 */
typedef enum {
	NRF24L01_IDLE = 0,          /**< IDLE nothing happened, start condition */
	NRF24L01_RX_DATA_READY = 2, /**< data in RX FIFO is ready for readout */
	NRF24L01_TX_DATA_SENT = 4,  /**< data in TX FIFO successfully transmitted */
	NRF24L01_MAX_RETRIES = 8,   /**< unable to send the data in the TX FIFO. The maximum retransmit count reached */
}NRF24L01_IRQ_status_t;


/**
 * @brief CRC Status
 */
typedef enum {
	NRF24L01_CRC_DISABLED = 0, /**< CRC is disabled it is not recommended to use this mode */
	NRF24L01_CRC_8,            /**< 8 Bit CRC checksum used */
	NRF24L01_CRC_16            /**< 16 Bit CRC checksum used recommended mode */
} NRF24L01_CRC_t;

/**
 * @brief nRF24L01 packet structure for communication
 */
typedef struct NRF24L01_Packet {
	uint8_t length;                       /**< payload length*/
	uint8_t address[NRF24L01_ADDR_LEN];   /**< payload address */
	char data[NRF24L01_MAX_PAYLOAD_BYTE]; /**< payload */
}NRF24L01_Packet_t;

/**
 * @brief RX configuration set-up
 */
typedef struct NRF24L01_RX_config {
	uint8_t pipes_enabled;                       /**< Bitmap of enabled pipes 0 disabled otherwise enabled */
	uint8_t rx_addr_p0[NRF24L01_ADDR_LEN];       /**< RX address for pipe 0 */
	uint8_t rx_addr_p1[NRF24L01_ADDR_LEN];       /**< RX address for pipe 1 */
	uint8_t rx_addr_p2_lsb;                      /**< LSB of RX address for pipe 2 */
	uint8_t rx_addr_p3_lsb;                      /**< LSB of RX address for pipe 3 */
	uint8_t rx_addr_p4_lsb;                      /**< LSB of RX address for pipe 4 */
	uint8_t rx_addr_p5_lsb;                      /**< LSB of RX address for pipe 5 */
	uint8_t payload_len[NRF24L01_NR_PIPES];      /**< Payload length for each pipe */
	uint8_t en_aa[NRF24L01_NR_PIPES];            /**< Bitmap for auto acknowledgement 0 disabled otherwise enabled*/
}NRF24L01_RX_config_t;

/**
 * @brief ARD - Auto retransmit delay
 */
typedef enum {
		NRF24L01_RETR_ARD_250 = 0x00,
		NRF24L01_RETR_ARD_500 = 0x10,
		NRF24L01_RETR_ARD_750 = 0x20,
		NRF24L01_RETR_ARD_1000 = 0x30,
		NRF24L01_RETR_ARD_1250 = 0x40,
		NRF24L01_RETR_ARD_1500 = 0x50,
		NRF24L01_RETR_ARD_1750 = 0x60,
		NRF24L01_RETR_ARD_2000 = 0x70,
		NRF24L01_RETR_ARD_2250 = 0x80,
		NRF24L01_RETR_ARD_2500 = 0x90,
		NRF24L01_RETR_ARD_2750 = 0xA0,
		NRF24L01_RETR_ARD_3000 = 0xB0,
		NRF24L01_RETR_ARD_3250 = 0xC0,
		NRF24L01_RETR_ARD_3500 = 0xD0,
		NRF24L01_RETR_ARD_3750 = 0xE0,
		NRF24L01_RETR_ARD_4000 = 0xF0,
} NRF24L01_TX_ARD_t;

/**
 * @brief ARC - Auto retransmit Count
 */
typedef enum {
		NRF24L01_RETR_ARC_0 = 0x00,
		NRF24L01_RETR_ARC_1 = 0x01,
		NRF24L01_RETR_ARC_2 = 0x02,
		NRF24L01_RETR_ARC_3 = 0x03,
		NRF24L01_RETR_ARC_4 = 0x04,
		NRF24L01_RETR_ARC_5 = 0x05,
		NRF24L01_RETR_ARC_6 = 0x06,
		NRF24L01_RETR_ARC_7 = 0x07,
		NRF24L01_RETR_ARC_8 = 0x08,
		NRF24L01_RETR_ARC_9 = 0x09,
		NRF24L01_RETR_ARC_10 = 0x0A,
		NRF24L01_RETR_ARC_11 = 0x0B,
		NRF24L01_RETR_ARC_12 = 0x0C,
		NRF24L01_RETR_ARC_13 = 0x0D,
		NRF24L01_RETR_ARC_14 = 0x0E,
		NRF24L01_RETR_ARC_15 = 0x0F,
} NRF24L01_TX_ARC_t;

/**
 * @brief Enable RX Pipe
 *
 */
typedef enum {
	NRF24L01_ERX_ALL = 0x3F,
	NRF24L01_ERX_P5 = 0x20,
	NRF24L01_ERX_P4 = 0x10,
	NRF24L01_ERX_P3 = 0x08,
	NRF24L01_ERX_P2 = 0x04,
	NRF24L01_ERX_P1 = 0x02,
	NRF24L01_ERX_P0 = 0x01,
	NRF24L01_ERX_NONE = 0x00
}NRF24L01_EN_RXADDR_t;

/**
 * @brief Initialization of the NRF24l01 driver
 * @details This function has to be called before any other
 * function can be called. This function performs the basic
 * configuration of the chip.
 *
 * @param spi_dev the number of the spi device
 * @return a pointer to the internal management structure of this driver. In case of an error this function returns NULL
 */
NRF24L01_t* nrf24l01_init(int spi_dev);

/**
 * @brief set-up of the PRX mode
 * @details In primary receive mode (PRX) The NRF24L01 can receive
 * data in up to 6 pipes. Two of the 6 Pipes are treated in a special way
 * These are the first two pipes. Which provide an up to five byte long
 * unique address for each pipe. The pipes two to five have the upper four bytes
 * in common with the pipe 1
 *
 * An example
 * ==========
 *         MSB                 LSB
 * RX_P0 : 0xE7 0xE7 0xE7 0xE7 0xE7
 * RX_P1 : 0xC2 0xC2 0xC2 0xC2 0xC2
 * RX_P2 : 0xC2 0xC2 0xC2 0xC2 0xC3
 * RX_P3 : 0xC2 0xC2 0xC2 0xC2 0xC4
 * RX_P4 : 0xC2 0xC2 0xC2 0xC2 0xC5
 * RX_P5 : 0xC2 0xC2 0xC2 0xC2 0xC6
 *
 * CONSTRAINTS
 * ===========
 *
 * - The least significant byte (LSB) must be unique for all six pipes.
 *   This ensures that none of the data pipes share the same address
 * - Do not use an address which start with 0xAA (1010 1010b) or 0x55 (0101 0101b)
 *   because the chip uses a one byte preamble for start of frame detection which
 *   toggles the bits. If the RX address starts with the same sequence the address
 *   may not be accepted under all conditions. See data-sheet "7.3.2 Address"
 * - Do not use addresses where the level only alters at one position like
 *   0xFF 0xFF 0xFF 0x00 0x00 this may also confuses the detector.
 *
 * The recommended standard format is an address length of 5 bytes.
 * The chip can handle various smaller address sizes this library is designed to
 * use 5 Bytes address length.
 *
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @param conf [in] pointer to a instance of NRF24L01_RX_config_t which contains the RX configuration
 */
void nrf24l01_rx_config(NRF24L01_t *hdl,NRF24L01_RX_config_t *conf);

/**
 * @brief Enables PRX mode and power up the device
 * @details This function has to be called to start the primary RX mode it uses
 * the RX configuration set by the function ::nrf24l01_rx_config. If not already done the
 * NRF24l01 is powered up. This function has to be called prior to read packet.
 *
 *
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 */
void nrf24l01_start_listening(NRF24L01_t *hdl);

/**
 * @brief Disables PRX mode and power down the device
 * @details This mode disables primary receive mode and enters stand by mode
 *
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 */
void nrf24l01_stop_listening(NRF24L01_t *hdl);

/**
 * @brief After a successful reception of a packet this function reads the data from the NRF24l01
 * @details It is recommended to call this function either after an RX data ready event or after
 * successfully  polling the function ::nrf24l01_rx_available the usage of an event driven approach
 * is recommended. Because it decreases the latency and therefore improves the reliability of transmits
 * across multiple chips.
 *
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @param data [inout] a pointer to a NRF24L01_Packet_t instance.
 *
 * @return number of bytes red from the chip. A negative value in case of an error
 */
int nrf24l01_read_packet(NRF24L01_t *hdl, NRF24L01_Packet_t *data);

/**
 * @brief Send a package to the receiver addressed provided in the package
 * @details This sends a package to the NRF24L01_Packet_t::address.
 * If auto acknowledgement is enabled the ::nrf24l01_write_packet function
 * automatically set the receive pipe P0 address to the same value as
 * the provided TX address which is needed for the ShockBurst technology.
 *
 *
 * CONSTRAINTS
 * ===========
 * - Sender and receiver have to use the same radio channel. The channel is configured
 *   by the function ::nrf24l01_set_channel
 * - Sender and receiver have to use the same CRC length. The CRC length can be adjusted
 *   by the function ::nrf24l01_set_crc_mode
 * - On the receiving device at least one pipe have to use the address configured in
 *   the NRF24L01_Packet_t::address. Double check the byte order on both devices especially
 *   if the counterpart uses a single data type like uint64_t. Then the bytes in the
 *   NRF24L01_Packet_t::address may have to be swapped.
 * - Both sender and receiver have to use the same address and payload length configuration.
 *   This driver uses a fixed address length of 5 bytes. The payload length is configured by
 *   NRF24L01_Packet_t::length.
 * - The receiver have to be in primary receive mode.
 *
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @param data [in] pointer to a NRF24L01_Packet_t instance
 *
 * @return number of bytes red from the chip. A negative value in case of an error
 */
int nrf24l01_write_packet(NRF24L01_t *hdl, NRF24L01_Packet_t *data);

/**
 * @brief Enables Power save mode
 * @details In this mode to data can be send nor received
 *
 * @param [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 */
void nrf24l01_power_down(NRF24L01_t *hdl);

/**
 * @brief Get receive power detect flag
 * @details This function can be used to scan a region of channels for activity of either
 * other NRF24l01 types of chips, W-LAN or bluetooth devices. This function can be useful
 * if you are interested in a scanner application which sweeps through all channels and detects
 * radio activity. It is recommended to use a channel with no disturbing activity.
 *
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @return In case of a radio activity on the configured channel true is returned otherwise false
 */
bool nrf24l01_get_rpd(NRF24L01_t *hdl);
/*
 * Information / statistics
 */
/**
 * @brief retrieve information about the tx status
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @param status [description]
 */
void nrf24l01_tx_status(NRF24L01_t *hdl,NRF24L01_tx_status_t *status);

/**
 * @brief dumps the content of all registers
 * @details this is for debugging purpose
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 */
void nrf24l01_dump_registers(NRF24L01_t *hdl);

/**
 * @brief returns the amount of available bytes in rx FIFO
 * @param  hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @return  the amount of bytes in RX FIFO a negative value if none available
 */
int8_t nrf24l01_rx_available(NRF24L01_t *hdl);

/*
 * TX Tweaks
 */

/**
 * @brief selects the radio channel
 * @details The channel must be equal on transmitter and receiver.
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @param channel [in] radio channel
 */
void nrf24l01_set_channel(NRF24L01_t *hdl,uint8_t channel);

/**
 * @brief get the current radio channel
 * @param  hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @return selected radio channel
 */
int nrf24l01_get_channel(NRF24L01_t *hdl);

/**
 * @brief Controls the retransmit parameters in primary transmit (PTX) mode
 *
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @param ard [in] auto retransmit delay, defines the delay between two successive transmits
 * @param arc [in] amount of retransmit attempts
 */
void nrf24l01_set_retransmit(NRF24L01_t *hdl,NRF24L01_TX_ARD_t ard, NRF24L01_TX_ARC_t arc);

/**
 * @brief configure the air data rate of the device
 * @details The data rate on receiver and transmitter have to match
 * @param  hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @param  data_rate desired data rate
 * @return status
 */
bool nrf24l01_set_air_data_rate(NRF24L01_t *hdl,NRF24L01_Air_Data_Rate_t data_rate);

/**
 * @brief adjustment of output power
 *
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @param power [in] desired output power
 */
void nrf24l01_set_output_power(NRF24L01_t *hdl,NRF24L01_Output_Power_t power);

/**
 * @brief selection of CRC mode
 *
 * @details a higher crc bit-rate may improve the reliability of the communication.
 *
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @param crc desired CRC mode
 */
void nrf24l01_set_crc_mode(NRF24L01_t *hdl, NRF24L01_CRC_t crc);

/**
 * @brief retrieve the selected CRC mode
 * @param  hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @return CRC mode
 */
NRF24L01_CRC_t nrf24l01_get_crc_mode(NRF24L01_t *hdl);

/**
 * @brief enable/disable auto acknowledgement for all pipes
 *
 * @details this might be useful for a scanner application.
 * @param hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @param enable set auto acknowledgement
 */
void nrf24l01_set_autoack(NRF24L01_t *hdl,bool enable);

/**
 * @brief retrieve the cause of an interrupt
 *
 * @details this is needed for the deferred procedure call.
 * @param  hdl [in] pointer to the internal data structure which is returned by ::nrf24l01_init
 * @return interrupt source
 */
NRF24L01_IRQ_status_t nrf24l01_get_irq_status(NRF24L01_t *hdl);


#ifdef __cplusplus
}
#endif

#endif /* NRF24L01_H__*/
